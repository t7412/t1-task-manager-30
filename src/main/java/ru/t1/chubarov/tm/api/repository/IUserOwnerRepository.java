package ru.t1.chubarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator) throws AbstractException;

    @Nullable
    M findOneById(@NotNull String userId, @Nullable String id) throws AbstractException;

    @Nullable
    M findOneByIndex(@NotNull String userId, @Nullable Integer index) throws AbstractException;

    @Nullable
    M removeOne(@NotNull String userId, @Nullable  M model) throws AbstractException;

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException;

    void removeAll(@Nullable String userId) throws AbstractException;

    @Nullable
    M add(@Nullable String userId, @Nullable M model) throws AbstractException;

    int getSize(@Nullable String userId) throws AbstractException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractException;

}
