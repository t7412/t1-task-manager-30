package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.exception.AbstractException;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CLEAR]");
        @NotNull final String userId = getUserId();
        getTaskService().removeAll(userId);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all task.";
    }

}
