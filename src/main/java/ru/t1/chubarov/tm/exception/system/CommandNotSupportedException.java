package ru.t1.chubarov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException(@NotNull String command) {
        super("Error. Command \"" + command + "\" not support.");
    }
}
